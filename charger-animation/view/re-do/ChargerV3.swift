//
//  ChargerV3.swift
//  charger-animation
//
//  Created by Fourth Dev on 10/11/2021.
//

import UIKit

class ChargerV3: UIView {
    
    let radius: CGFloat = 120
    var percent: CGFloat = 0.9
    let circleLineWidth: CGFloat = 20
    let gradientHeight: CGFloat = 200
    
    var layer1 = CALayer()
    let layer2 = CALayer()
    let text = CenterTextLayer()
    
    var didLoad = false
    
    override func draw(_ rect: CGRect) {
        let circleCenter = CGPoint(x: bounds.midX, y: bounds.midY - 100)
        
        func initLayerWithOpacity(_ opacity: CGFloat) -> CALayer {
            let circle = CAShapeLayer()
            let downSizedPercent = percent == 1 ? 0.99 : percent * 0.96
            let circlePath = UIBezierPath(arcCenter: circleCenter, radius: radius, startAngle: -.pi/2 + .pi*(1 - downSizedPercent), endAngle: -.pi/2 - .pi*(1 - downSizedPercent), clockwise: true)
            circle.path = circlePath.cgPath
            circle.fillColor = nil
            circle.strokeColor = UIColor.gray.withAlphaComponent(alpha).cgColor
            circle.lineWidth = circleLineWidth
            circle.lineCap = .round
            
            let line = CALayer()
            let lineY = circleCenter.y + radius + circleLineWidth
            line.frame = CGRect(x: bounds.midX - 3, y: lineY, width: 6, height: bounds.height - lineY)
            line.backgroundColor = UIColor.gray.cgColor
            line.cornerRadius = 3
            
            let layer = CALayer()
            layer.addSublayer(circle)
            layer.addSublayer(line)
            return layer
        }
        
        if !didLoad {
            let fullCircle = CAShapeLayer()
            fullCircle.path = UIBezierPath(ovalIn: CGRect(origin: circleCenter, size: .zero).insetBy(dx: -radius, dy: -radius)).cgPath
            fullCircle.fillColor = nil
            fullCircle.strokeColor = UIColor.gray.cgColor
            fullCircle.lineWidth = circleLineWidth
            layer.addSublayer(fullCircle)
            
        }
        
        layer1 = initLayerWithOpacity(0.2)
        
        
        text.string = "\(Int(percent*100))%"
        text.font = UIFont.systemFont(ofSize: 20)
        text.fontSize = 20
        text.foregroundColor = UIColor.green.cgColor
        text.alignmentMode = .center
        text.frame = CGRect(x: circleCenter.x - 30, y: circleCenter.y - 20, width: 60, height: 40)
        layer.addSublayer(text)
        
        layer.addSublayer(layer1)
        layer.addSublayer(layer2)
        let maskLayer = initLayerWithOpacity(1)
        layer2.mask = maskLayer
        if didLoad { return }
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { _ in
            let gradient = CAGradientLayer()
            gradient.colors = [
                UIColor.green.withAlphaComponent(0).cgColor,
                UIColor.green.cgColor,
                UIColor.green.withAlphaComponent(0).cgColor
            ]
            gradient.frame = CGRect(x: 0, y: self.bounds.height - self.gradientHeight, width: self.bounds.width , height: self.gradientHeight)
            let anim = CABasicAnimation(keyPath: "transform.translation.y")
            anim.fromValue = self.bounds.height
            anim.toValue = -(self.bounds.midY - 100 + self.radius + self.circleLineWidth + self.gradientHeight)
            anim.duration = 0.5
            gradient.add(anim, forKey: nil)
            Timer.scheduledTimer(withTimeInterval: anim.duration, repeats: false) { _ in
                gradient.removeFromSuperlayer()
            }
            self.layer2.addSublayer(gradient)
        }
        didLoad = true
    }

}

class CenterTextLayer: CATextLayer {
    override func draw(in ctx: CGContext) {
        let height = self.bounds.height
        let fontSize = self.fontSize
        let yDiff = (height - fontSize)/2 - 3
        ctx.saveGState()
        ctx.translateBy(x: 0, y: yDiff)
        super.draw(in: ctx)
        ctx.restoreGState()
    }
}

extension ChargerV3: SliderViewDelegate {
    func alphaChanged(_ alpha: Double) {
        percent = alpha
        setNeedsDisplay()
    }
}
