//
//  ViewController.swift
//  charger-animation
//
//  Created by Fourth Dev on 08/11/2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var sliderView: SliderView!
    @IBOutlet weak var chargerView: ChargerV3!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        /*
         3 : round, half round, line
         */
        chargerView.percent = sliderView.value
        sliderView.delegate = chargerView
        
    }
}

