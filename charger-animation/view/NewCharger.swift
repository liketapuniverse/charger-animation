//
//  NewCharger.swift
//  charger-animation
//
//  Created by Fourth Dev on 09/11/2021.
//

import UIKit

class NewCharger: UIView {

    var didLoad = false
    let circleLineWidth: CGFloat = 25
    let lineWidth: CGFloat = 6
    let radius: CGFloat = 150
    var percent: CGFloat = 0.90
    let gradientHeight: CGFloat = 200
    
    override func draw(_ rect: CGRect) {
        
        let circleCenter = CGPoint(x: bounds.midX, y: bounds.midY - 100)
        func initContent(_ opacity: CGFloat) -> CALayer {
            let circle = CAShapeLayer()
            let circlePath = UIBezierPath(
                arcCenter: circleCenter,
                radius: radius,
                startAngle: -.pi/2 + .pi*(1-percent),
                endAngle: -.pi/2 - .pi*(1-percent),
                clockwise: true)
            circle.path = circlePath.cgPath
            circle.fillColor = nil
            circle.strokeColor = UIColor.green.withAlphaComponent(opacity).cgColor
            circle.lineWidth = circleLineWidth
            
            let line = CALayer()
            let lineY = circleCenter.y + radius + circleLineWidth/2 + 10
            line.frame = CGRect(x: circleCenter.x - 3, y: lineY, width: 6, height: bounds.height - lineY)
            line.backgroundColor = UIColor.gray.cgColor
            line.cornerRadius = 3
            
            let layer = CALayer()
            layer.addSublayer(circle)
            layer.addSublayer(line)
            return layer
        }
        let layer1 = initContent(0.2)
        
        let layer2 = CALayer()
        layer2.mask = initContent(1)
        
        
        let fullCircle = CAShapeLayer()
        fullCircle.path = UIBezierPath(ovalIn: CGRect(origin: circleCenter, size: .zero).insetBy(dx: -radius, dy: -radius)).cgPath
        fullCircle.fillColor = nil
        fullCircle.strokeColor = UIColor.gray.cgColor
        fullCircle.lineWidth = circleLineWidth

        
        layer.addSublayer(fullCircle)
        layer.addSublayer(layer1)
        layer.addSublayer(layer2)
        
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
            let gradient = CAGradientLayer()
            gradient.frame = CGRect(x: 0, y: self.bounds.height, width: self.bounds.width, height: self.gradientHeight)
            gradient.colors = [
                UIColor.green.withAlphaComponent(0).cgColor,
                UIColor.green.cgColor,
                UIColor.green.withAlphaComponent(0).cgColor
            ]
            layer2.addSublayer(gradient)
            let anim = CABasicAnimation(keyPath: "transform.translation.y")
            anim.fromValue = 0
            anim.toValue = -(self.gradientHeight + self.bounds.height - (circleCenter.y - self.radius))
            anim.duration = 0.5
            gradient.add(anim, forKey: nil)
            Timer.scheduledTimer(withTimeInterval: anim.duration, repeats: false) { _ in
                gradient.removeFromSuperlayer()
            }
        }
    }
}
