//
//  ChargerVer2.swift
//  charger-animation
//
//  Created by Fourth Dev on 09/11/2021.
//

import UIKit

class ChargerVer2: UIView {

    let radius: CGFloat = 120
    let circleLineWidth: CGFloat = 25
    var percent: CGFloat = 0.05
    let gradientHeight: CGFloat = 200
    // near 0.98 > round to 1
    var didLoad = false
    
    let layer2 = CALayer()
    
    
    override func draw(_ rect: CGRect) {
        percent = percent == 1 ? 0.99 : percent * 0.96
//        if didLoad { return }
        let circleCenter = CGPoint(x: bounds.midX, y: bounds.midY - 100)
        
        func initContentWholeLayer(_ opacity: CGFloat) -> CALayer {
            let circle = CAShapeLayer()
            let circlePath = UIBezierPath(arcCenter: circleCenter, radius: radius, startAngle: -.pi/2 + .pi*(1 - percent), endAngle: -.pi/2 - .pi*(1 - percent), clockwise: true) // change here
            circle.path = circlePath.cgPath
            circle.fillColor = nil
            circle.strokeColor = UIColor.green.withAlphaComponent(opacity).cgColor
            circle.lineWidth = circleLineWidth
            circle.lineCap = .round
            
            let line = CALayer()
            let lineY = circleCenter.y + radius + circleLineWidth + 10
            line.frame = CGRect(x: bounds.midX - 3, y: lineY, width: 6, height: bounds.height - lineY)
            line.backgroundColor = UIColor.gray.cgColor
            line.cornerRadius = 3
            
            let layer = CAShapeLayer()
            layer.addSublayer(circle)
            layer.addSublayer(line)
            return layer
        }
        
        // khoi tao o tren
        let layer1 = initContentWholeLayer(0.2)
        
        let fullCircle = CAShapeLayer()
        fullCircle.path = UIBezierPath(ovalIn: CGRect(origin: circleCenter, size: .zero).insetBy(dx: -radius, dy: -radius)).cgPath
        fullCircle.fillColor = nil
        fullCircle.lineWidth = circleLineWidth
        fullCircle.strokeColor = UIColor.gray.cgColor
        
        
        
        
        layer.addSublayer(fullCircle)
        layer.addSublayer(layer1)
        layer.addSublayer(layer2)  // as mask layer
        
        //layer 2 as mask container
        layer2.mask = initContentWholeLayer(1)
        
        if didLoad {return}
        // mask
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { _ in
            let gradient = CAGradientLayer()
            gradient.frame = CGRect(x: 0, y: self.bounds.maxY - self.gradientHeight, width: self.bounds.width, height: self.gradientHeight)
            gradient.colors = [
                UIColor.green.withAlphaComponent(0).cgColor,
                UIColor.green.cgColor,
                UIColor.green.withAlphaComponent(0).cgColor
            ]
            //add sub
            self.layer2.addSublayer(gradient) // mask one
            
            let anim = CABasicAnimation(keyPath: "transform.translation.y")
            anim.fromValue = 0
            anim.toValue = -(circleCenter.y + self.radius + self.circleLineWidth)
            anim.duration = 0.5
            gradient.add(anim, forKey: nil)
            Timer.scheduledTimer(withTimeInterval: anim.duration, repeats: false) { _ in
                gradient.removeFromSuperlayer()
            }
        }
        didLoad = true
    }
    
    func reLoadView() {
        setNeedsDisplay()
    }
}

extension ChargerVer2: SliderViewDelegate {
    func alphaChanged(_ alpha: Double) {
        percent = alpha
        layer.sublayers?.forEach({ sub in
            sub.removeFromSuperlayer()
        })
        setNeedsDisplay()
    }
}
