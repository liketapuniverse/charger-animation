//
//  AnimatedView.swift
//  charger-animation
//
//  Created by Fourth Dev on 08/11/2021.
//

import UIKit

class AnimatedView: UIView {
    let radius: Double = 100
    var percent: Double = 0.96
    let startColor = UIColor(red: 46, green: 56, blue: 32)
    var timer = Timer()
    
    let gradient = CAGradientLayer()
    let containerForMask = CALayer()
    let maskLayer = CALayer()
    let gradientHeight: Double = 300
    var didLoad = false
    var movedYForGradient: Double = 0
    
    override func draw(_ rect: CGRect) {
        if !didLoad {
            let layer1 = CALayer()
            let circle1 = initCircle()
            let line1 = initLine()
            layer1.addSublayer(circle1)
            layer1.addSublayer(line1)
            layer.addSublayer(layer1)
            
            let text = LCTextLayer()
            text.string = "\(Int(percent*100))%"
            text.foregroundColor = UIColor.green.cgColor
            text.font = UIFont.systemFont(ofSize: 15)
            text.fontSize = 20
            text.frame = CGRect(x: bounds.midX - 25, y: bounds.midY - 100 - 25, width: 50, height: 50)
            text.alignmentMode = .center
            layer1.addSublayer(text)
            
            let circle2 = initCircle()
            let line2 = initLine()
            maskLayer.addSublayer(circle2)
            maskLayer.addSublayer(line2)
            
            let time: Double = 0.01
            timer = Timer.scheduledTimer(withTimeInterval: time, repeats: true, block: { _ in
                let gradient = CAGradientLayer()
                self.movedYForGradient += 10
                if self.movedYForGradient == 1000 { self.movedYForGradient = 0}
                
                gradient.frame = CGRect(x: self.bounds.midX - 150, y: self.bounds.maxY, width: 300, height: self.gradientHeight)
                gradient.colors = [
                    UIColor.green.withAlphaComponent(0).cgColor,
                    UIColor.green.cgColor,
                    UIColor.green.withAlphaComponent(0).cgColor
                ]
                self.containerForMask.addSublayer(gradient)
                self.containerForMask.mask = self.maskLayer
                
                let anim = CABasicAnimation(keyPath: "position.y")
                anim.duration = time
                anim.fromValue = self.bounds.maxY
                anim.toValue = self.bounds.maxY - self.movedYForGradient
                gradient.add(anim, forKey: nil)
                
                Timer.scheduledTimer(withTimeInterval: anim.duration, repeats: false) { _ in
                    gradient.removeFromSuperlayer()
                }
            })
            
            layer.addSublayer(containerForMask)
        }
        didLoad = true
    }
    
    private func initCircle() -> CALayer {
        let circle = CAShapeLayer()
        let circleY = bounds.midY - 100
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: bounds.midX, y: circleY), radius: radius, startAngle: .pi*(0.5 - percent), endAngle: .pi*(0.5 + percent) , clockwise: true)
        circle.path = circlePath.cgPath
        circle.fillColor = nil
        circle.strokeColor = UIColor.gray.cgColor
        circle.lineWidth = 10
        circle.lineCap = .round
        return circle
    }

    private func initLine() -> CALayer {
        let line = CALayer()
        let circleY = bounds.midY - 100
        let startY: Double = circleY + radius + 15
        line.frame = CGRect(x: bounds.midX - 3, y: startY, width: 6, height: bounds.height - startY )
        line.backgroundColor = UIColor.gray.cgColor
        line.cornerRadius = 3
        return line
    }
}


extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
}

class LCTextLayer: CATextLayer {
    override func draw(in context: CGContext) {
        let height = self.bounds.size.height
        let fontSize = self.fontSize
        let yDiff = (height-fontSize)/2 - 3
        context.saveGState()
        context.translateBy(x: 0, y: yDiff)
        super.draw(in: context)
        context.restoreGState()
    }
}
